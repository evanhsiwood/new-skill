# New Skills of Java

This is and will be a place to house demos of new skills of java.

# 0.4.0

- Jersey toy modules, both grizzly and webapps

- mybatis modules

- groovy test modules, integrated with mockito

# 0.1.0

- A toy module of grizzly

