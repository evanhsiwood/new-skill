import spock.lang.*;

class MathSpec extends Specification {

    @Unroll
    def "maximum of two numbers"(int a, int b, int c) {

        expect:
        Math.max(a, b) == c

        where:
        a | b  | c
        1 | 3  | 3
        4 | 2  | 4
        8 | 13 | 13
    }
}