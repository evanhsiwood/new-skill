import com.evanhsiwood.new_skill.toy_mybatis.domain.Account
import com.evanhsiwood.new_skill.toy_mybatis.persistence.AccountMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.*

@ContextConfiguration(locations =
"classpath*:applicationContext.xml")
class AccountMapperSpec extends Specification {

    @Autowired
    AccountMapper accountMapper

    def "I should get account by his dianping's account"() {

        given: "a dianping user called shanshan.xi"
        def account = new Account();
        account.ad = "shanshan.xi"

        when: "find a account by AccountService"
        def account1 = accountMapper.getAccountByEmployeeNoOrGmailOrAd(account)

        then:
        account1.ad == "shanshan.xi"
        println(account1.toString())
    }

}
