import com.evanhsiwood.new_skill.toy_mybatis.domain.Account
import com.evanhsiwood.new_skill.toy_mybatis.persistence.AccountMapper
import com.evanhsiwood.new_skill.toy_mybatis.service.AccountService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.util.ReflectionTestUtils
import spock.lang.*
import org.mockito.*
import org.mockito.Matchers.*

import static org.mockito.Matchers.isA
import static org.mockito.Mockito.when

@ContextConfiguration(locations =
"classpath*:applicationContext.xml")
class AccountServiceSpec extends Specification {

//    @Autowired
//    AccountService accountService
//    AccountMapper accountMapper = Mock()

    @InjectMocks
    private AccountService accountService
    @Mock
    private AccountMapper accountMapper

    def setup() {
        MockitoAnnotations.initMocks(this);

        def account = new Account(
                id: 2,
                name: "Evan Wood",
                ad: "evanhsiwood",
                gmail: "evanhsiwood@gmail.com",
                employeeNo: "8001036")
//        accountMapper.getAccountByEmployeeNoOrGmailOrAd(_) >> { account }

        when(accountMapper.getAccountByEmployeeNoOrGmailOrAd(isA(Account))).thenReturn(account)
//        accountService.setAccountMapper(accountMapper)
    }

    def "AccountService should get account by its name"() {
        given:
        // no use, seems that autowired bean override the reflection way
//        ReflectionTestUtils.setField(accountService, "accountMapper", accountMapper)
        def name = "evanhsiwood"

        when:
        def account1 = accountService.getAccount(name)

        then:
        account1.ad == "evanhsiwood"
    }

    def "AccountService#getAccount should call accountMapper.getAccountByEmployeeNoOrGmailOrAd once"() {
        given:

        when:
        accountService.getAccount(_ as String)

        then:
        1 * accountMapper.getAccountByEmployeeNoOrGmailOrAd(_ as Account)
    }

}
