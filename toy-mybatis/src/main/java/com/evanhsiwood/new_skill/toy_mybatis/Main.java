package com.evanhsiwood.new_skill.toy_mybatis;

import com.evanhsiwood.new_skill.toy_mybatis.domain.Account;
import com.evanhsiwood.new_skill.toy_mybatis.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class Main {

    public static void main(String[] args) throws IOException {

        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        Main p = context.getBean(Main.class);
        p.start(args);
    }

    @Autowired
    private AccountService accountService;

    private void start(String[] args) {
        Account account = accountService.getAccount("evanhsiwood");

        Account account1 = new Account();
        account1.setId(2);
        account1.setName("奚杉杉2");
        account1.setEmployeeNo("9001039");
        account1.setGmail("pieux.xi@gmail.com");
        account1.setAd("pieux.xi");

        accountService.insertAccount(account1);
        Account account2 = accountService.getAccount("pieux.xi");

        account1.setName("奚杉杉3");
        accountService.updateAccount(account1);
        Account account3 = accountService.getAccount("pieux.xi");

        System.out.println(account.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

    }
}
