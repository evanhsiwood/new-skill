package com.evanhsiwood.new_skill.toy_mybatis.domain;

import com.google.common.base.Objects;

import java.io.Serializable;

public class Account implements Serializable {

    private static final long serialVersionUID = 8922261801337621719L;

    private int id;
    private String name;
    private String employeeNo;
    private String gmail;
    private String ad;

    @Override
    public String toString() {
        return Objects.toStringHelper(getClass())
                .add("Id", id)
                .add("Name", name)
                .add("EmployeeNo", employeeNo)
                .add("Gmail", gmail)
                .add("AD", ad)
                .toString();
    }


    // always set getter/setter at the very bottom to make code cleaner

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

}
