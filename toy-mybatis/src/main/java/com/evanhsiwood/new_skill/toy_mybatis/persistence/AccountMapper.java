package com.evanhsiwood.new_skill.toy_mybatis.persistence;

import com.evanhsiwood.new_skill.toy_mybatis.domain.Account;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountMapper {

    Account getAccountByEmployeeNoOrGmailOrAd(Account account);

    void insertAccount(Account account);

    void updateAccount(Account account);

}