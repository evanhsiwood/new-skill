package com.evanhsiwood.new_skill.toy_mybatis.service;

import com.evanhsiwood.new_skill.toy_mybatis.domain.Account;
import com.evanhsiwood.new_skill.toy_mybatis.persistence.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountService {

    @Autowired
    private AccountMapper accountMapper;

    // for test right now
//    public void setAccountMapper(AccountMapper accountMapper) {
//        this.accountMapper = accountMapper;
//    }

    public Account getAccount(String dpAccount) {
        Account account = new Account();
        account.setEmployeeNo(dpAccount);
        account.setGmail(dpAccount);
        account.setAd(dpAccount);
        return accountMapper.getAccountByEmployeeNoOrGmailOrAd(account);
    }

    @Transactional
    public void insertAccount(Account account) {
        accountMapper.insertAccount(account);
    }

    @Transactional
    public void updateAccount(Account account) {
        accountMapper.updateAccount(account);
    }

}
