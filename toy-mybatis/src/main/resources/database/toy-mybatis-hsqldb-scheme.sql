-- drop table Account;

create table Account (
    id int not null,
    name varchar(80) null,
    employeeNo varchar(80) null,
    gmail varchar(80) null,
    ad varchar(80) null,
    constraint pk_account primary key (id)
);