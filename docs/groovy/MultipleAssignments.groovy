def splitName(fullName) {
  fullName.split ' '
}

def(firstName, lastName) = splitName('Shanks Hsi')
println "$lastName, $firstName $lastName"