def log(x, base = 10) {
  Math.log(x) / Math.log(base)
}

println log(1024)
println log(1024, 2)
println log(1024, 10)

def task(name, String[] details) {
  println "$name - $details"
}

task('call', '123-456-78')
task('idle')
