Why learn Groovy?
===

# PART1 Beginning Groovy

## CHAPTER 1 Getting Started

### 1.1 Installing Groovy

`$GROOVY_HOME`

### 1.2 Installing and Managing Groovy Versions

GVM

### 1.3 Test-Drive Using groovysh

`groovysh`

### 1.4 Using groovyConsole

A GUI version. `cmd+R` or `cmd+Enter` to run.

### 1.5 Running Groovy on the Command Line

```
> cat Hello.groovy
  println "Hello Groovy!"
  > groovy Hello
  Hello Groovy!
  >
```

```
groovy -e "println 'hello'"
```

### 1.6 Using an IDE

## CHAPTER 2 Groovy for Java Eyes

### 2.1 From Java to Groovy

**Default Imports**

Groovy automatically imports the following Java packages: java.lang,
java.util, java.io, and java.net. It also imports the classes
java.math.BigDecimal and java.math.BigInteger. In addition, the Groovy
packages groovy.lang and groovy.util are imported.


```
for(int i = 0; i < 3; i++) {
    System.out.print("ho ");
}

System.out.println("Merry Groovy!");

```

Groovy understands println() because it has been added on `java.lang.Object`.
It also has a lighter form of the for loop that uses the `Range`
object, and Groovy is lenient with parentheses.


```
for(i in 0..2) {print 'ho '}
println 'Merry Groovy!'
```

**Ways to Loop**

Groovy provides quite a number of elegant ways to iterate; let’s look at a few.

```
0.upto(2) {print "$it "}
```

closure as a parementer of `upto`.

`$string` compute the value other than the literal value. Like lisp.


start at 0

```
3.times { print "$it "}
```

skip values while looping

```
0.step(10, 2) { print "$it "}
```

**A Quick Look at the GDK**

To program in Groovy, we’re not forced to learn a new set of classes
and libraries. Groovy extends the powerful JDK by adding convenience
methods to various classes. These extensions are available in the
library called the GDK, or the Groovy JDK
(http://groovy.codehaus.org/groovy-jdk).

**safe-navigation operator**

One such feature is the safe navigation operator (?.). It eliminates the mundane check for null.

**Exception Handling**

Groovy does not force us to handle exceptions that we don’t want to
handle or that are inappropriate at the current level of code. Any
exception we don’t handle is automatically passed on to a higher
level.

```
def openFile(fileName) {
  new FileInputStream(fileName)
}

try {
  openFile("nonexistentfile")
} catch(ex) {
  println "Ops: " + ex
}
```
With the catch(ex) without any type in front of the variable ex, we
  can catch just about any exception thrown our way. Beware: this
  doesn’t catch Errors or Throwables other than Exceptions. To catch
  all of them, use catch(Throwable throwable).

**Groovy as Lightweight Java**

- The return statement is almost always optional
- The semicolon (;) is almost always optional, though we can use it to
  sepa- rate statements
- Methods and classes are public by default.
- The ?. operator dispatches calls only if the object reference is not
  null.
- We can initialize JavaBeans using named parameters
- We’re not forced to catch exceptions that we don’t care to handle.
  They get passed to the caller of our code.
- We can use this within static methods to refer to the Class object.
  In the next example, the learn() method returns the class so we can
  chain calls:

```
class Wizard {
    def static learn(trick, action) {
        //...
        this
    }
}

Wizard.learn('alohomora', {/*...*/})
    .learn('expelliarmus', {/*...*/})
    .learn('lumos', {/*...*/})
```

### 2.2 JavaBeans

Java objects would be considered JavaBeans if they followed certain
conventions to expose their properties.

To be fair, the intent of JavaBean is noble—it made component-based
 development, application assembly, and integration practical and
 paved the way for exceptional integrated development environment
 (IDE) and plug-in development.

```
//Java code
public class Car {
    private int miles;
    private final int year;
    public Car(int theYear) { year = theYear; }
    public int getMiles() { return miles; }
    public void setMiles(int theMiles) { miles = theMiles; }
    public int getYear() { return year; } public static void main(String[] args) {
        Car car = new Car(2008);
        System.out.println("Year: " + car.getYear());
        System.out.println("Miles: " + car.getMiles());
        System.out.println("Setting miles"); car.setMiles(25);
        System.out.println("Miles: " + car.getMiles());
    }
}
```

```
class Car {
  def miles = 0
  final year

  Car(theYear) { year = theYear }
}

Car car = new Car(2008)
println "Year: $car.year"
println "Miles: $car.miles"
println 'Setting miles'
car.miles = 25
println "Miles: $car.miles"
```

`def` declared a property in this context. We can declare properties by
either using def as in the example or giving the type (and optional
value) as in `int miles` or  `int miles = 0`.

Groovy quietly created a getter and a setter method behind the scenes
(just like how a constructor is created in Java if we don’t write any
constructor).

When we call miles in our code, we’re not referencing a field;
instead, we’re calling the getter method for the miles property.

To make a property read-only, we declare it final, just like in Java.
Optionally, we can add a type information to the declaration. Groovy
provides a getter and no setter in this case.


### 2.3 Flexible Initialization and Named Arguments

```
class Robot {
  def type, height, width
  def access(location, weight, fragile) {
    println "Received fragile? $fragile, weight: $weight, loc: $location"
  }
}

robot = new Robot(type: 'arm', width: 10, height: 40)
println "$robot.type, $robot.height, $robot.width"

robot.access(x: 30, y: 20, z: 10, 50, true)
//You can change the order
robot.access(50, true, x: 30, y: 20, z: 10)
```

If the number of arguments we send is greater than the number of
parameters the method expects and if the excess arguments are
name-value pairs, then Groovy assumes the method’s first parameter is
a Map and groups all the name-value pairs in the arguments together as
values for the first parameter.

 如果实参多于形参，并且实参有 name-values 对，则形参的第一个参数被
 Groovy 认为是 map，所有的 name-values 对放到 map 里。

The arguments for the map can be moved further down in the arguments
list if we desire, like in the second call to the access() method.

 并且， 作为 map 的参数可以放到后面。

> 了解下，不要轻易使用。 看起来很混乱。

显式定义下：

```
def access(Map location, weight, fragile) {
    print "Received fragile? $fragile, weight: $weight, loc: $location"
}
```






### 2.4 Optional Parameters

```
def log(x, base = 10) {
  Math.log(x) / Math.log(base)
}

println log(1024)
println log(1024, 2)
println log(1024, 10)

def task(name, String[] details) {
  println "$name - $details"
}

task('call', '123-456-78')
task('idle')
```
### 2.5 Using Multiple Assignments

Passing multiple arguments to methods is commonplace in many
programming languages. Returning multiple results from functions, on
the other hand, is not that common, though it can be quite convenient.
We can return multiple results from functions and assign them to
multiple variables in one shot. Simply return an array and use
comma-separated variables wrapped in parentheses on the left side of
the assignment.

又是一个语法糖。返回多值，其实是返回一个数组。一次性赋值，是赋值给用
`()`包含的变量里面。

```
def splitName(fullName) {
  fullName.split ' '
}

def(firstName, lastName) = splitName('Shanks Hsi')
println "$lastName, $firstName $lastName"
```


### 2.6 Implementing Interfaces
