def openFile(fileName) {
  new FileInputStream(fileName)
}

try {
  openFile("nonexistentfile")
} catch(ex) {
  println "Ops: " + ex
}